import 'package:flutter/material.dart';
import 'main.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('BUU Login'),
      ),
      body: buildBodyLogin(context),
    );
  }

  Widget buildBodyLogin(context) {
    return SingleChildScrollView(
        child:Center(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'BUU',
              style: TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
            ),
            Text(
              'Burapha University',
              style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Username'),
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
            SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MyApp(),
                        ));
                  },
                  child: Text('Login'),
                ),
                SizedBox(height: 10.0),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.redAccent,
                  ),
                  onPressed: () {},
                  child: Text(
                    'Forget Password',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),)
    ;

  }
}
