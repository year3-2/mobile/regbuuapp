import 'package:flutter/material.dart';

class Tables extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('ตารางเรียน'),
      ),
      body: BuildBodyTable(context),
    );
  }

  Widget BuildBodyTable(context) {
     final screenHeight = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                height:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? screenHeight*0.5
                        : screenHeight*0.9,
                child: Card(
                  color: Colors.grey[50],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 8,
                  child: Container(
                    padding: EdgeInsets.only(left: 16.0, top: 16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.amber[600])),
                        SizedBox(height: 4),
                        Text("ชื่อ",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('ณัฏฐชัย จันทร์ศรี',
                            style: TextStyle(fontSize: 14)),
                        Text("สถานภาพ",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('กำลังศึกษา', style: TextStyle(fontSize: 14)),
                        Text("คณะ",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('วิทยาการสารสนเทศ',
                            style: TextStyle(fontSize: 14)),
                        Text("หลักสูตร",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                            'วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติวิทยาศาสตรบัณฑิต (วท.บ.)',
                            style: TextStyle(fontSize: 14)),
                        Text("อ. ที่ปรึกษา",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                            'ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                            style: TextStyle(fontSize: 14)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 2, vertical: 10),
              width: MediaQuery.of(context).size.width,
              height: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                image: DecorationImage(
                  image: NetworkImage(
                      'https://media.discordapp.net/attachments/1043925948707381278/1070302237911433270/image.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 2, vertical: 10),
              width: MediaQuery.of(context).size.width,
              height: 250,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                image: DecorationImage(
                  image: NetworkImage(
                      'https://media.discordapp.net/attachments/1043925948707381278/1070302329049448468/image.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
