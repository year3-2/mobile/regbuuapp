import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'drawerpage.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
    DevicePreview(
      enabled: true,
      builder: (context) => const MyApp(),
    ),
  );
}

//Create widget
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      useInheritedMediaQuery: true,
      debugShowCheckedModeBanner: false,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      title: "Burapha University",
      home: Scaffold(
        drawer: DrawerPage(),
        appBar: buildAppBarWidget(),
        body: BuildBodyWidget(context),
      ),
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        canvasColor: Colors.grey[300],
      ),
    );
  }

  AppBar buildAppBarWidget() {
    return AppBar(
      title: Text(
        "Register Burapha University",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget BuildBodyWidget(context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BuildButtonlist(),
          Text("ประกาศเรื่อง",
              style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  backgroundColor: Colors.grey)),
          SizedBox(height: 4),
          BuildCardWidget2(context),
          SizedBox(height: 4),
          Text("ติดต่อกองทะเบียน",
              style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  backgroundColor: Colors.grey)),
          BuildCardWidget(context),
          SizedBox(height: 4),
          Text("การชำระเงิน",
              style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  backgroundColor: Colors.grey)),
          BuildCardWidget3(context),
        ],
      ),
    );
  }

  Widget BuildButtonlist() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.redAccent,
            ),
            onPressed: () {},
            child: Text(
              'แนะนำการลงทะเบียน',
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.greenAccent,
            ),
            onPressed: () {},
            child: Text(
              'เกี่ยวกับการลงทะเบียน',
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blue,
            ),
            onPressed: () {},
            child: Text(
              'Facebook',
            ),
          ),
        ],
      ),
    );
  }

  Widget BuildCardWidget3(context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2, vertical: 10),
      width: MediaQuery.of(context).size.width,
      height: 300,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        image: DecorationImage(
          image: NetworkImage(
              'https://media.discordapp.net/attachments/1043925948707381278/1070279917335494716/pay652.png'),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget BuildCardWidget2(context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2, vertical: 10),
      width: MediaQuery.of(context).size.width,
      height: 350,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        image: DecorationImage(
          image: NetworkImage(
              'https://media.discordapp.net/attachments/1043925948707381278/1070272699978502216/grad652.png?width=1008&height=702'),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget BuildCardWidget(context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2, vertical: 10),
      width: MediaQuery.of(context).size.width,
      height: 180,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        image: DecorationImage(
          image: NetworkImage(
              'https://media.discordapp.net/attachments/642334183112310796/1070251034162778212/Line.jpg'),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
