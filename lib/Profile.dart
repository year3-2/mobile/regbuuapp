import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ข้อมูลนิสิต'),
      ),
      body: buildBodyProfile(context),
    );
  }

  Widget buildBodyProfile(context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                height:
                MediaQuery.of(context).orientation == Orientation.portrait
                    ? screenHeight*0.4
                    : screenHeight*0.9,
                child: Card(
                  color: Colors.grey[50],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 8,
                  child: Container(
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  "https://media.discordapp.net/attachments/1043925948707381278/1070240549421994044/IMG_0375.jpg"),
                            ),
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Profile",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.amber[600])),
                              SizedBox(height: 4),
                              Text("User type",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 4),
                              Text('Student', style: TextStyle(fontSize: 14)),
                              Text("ID",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 4),
                              Text('63160004', style: TextStyle(fontSize: 14)),
                              Text("Faculty (EN)",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 4),
                              Text('Informatics',
                                  style: TextStyle(fontSize: 14)),
                              Text("Major",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 4),
                              Text('B.Sc. (Computer Science)',
                                  style: TextStyle(fontSize: 14)),
                              Text('Year ',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 4),
                              Text('3', style: TextStyle(fontSize: 14)),
                              SizedBox(height: 4),
                              Text('GPA ',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 4),
                              Text('3.30', style: TextStyle(fontSize: 14)),
                              SizedBox(height: 4),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                height:
                MediaQuery.of(context).orientation == Orientation.portrait
                    ? screenHeight*0.4
                    : screenHeight*0.9,
                child: Card(
                  color: Colors.grey[50],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 8,
                  child: Container(
                    padding: EdgeInsets.only(left: 16.0, top: 16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Profile",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.amber[600])),
                        SizedBox(height: 4),
                        Text("Name(EN)",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('Natthachai Jansri',
                            style: TextStyle(fontSize: 14)),
                        Text("Name(TH)",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('ณัฏฐชัย จันทร์ศรี',
                            style: TextStyle(fontSize: 14)),
                        Text("Birthdate",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('16 Sep 2001', style: TextStyle(fontSize: 14)),
                        Text("Gender",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('Male', style: TextStyle(fontSize: 14)),
                        Text("Nationality",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('Thai', style: TextStyle(fontSize: 14)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height:
              MediaQuery.of(context).orientation == Orientation.portrait
                  ? screenHeight*0.4
                  : screenHeight*0.9,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                height: MediaQuery.of(context).size.height * 0.3,
                child: Card(
                  color: Colors.grey[50],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 8,
                  child: Container(
                    padding: EdgeInsets.only(left: 16.0, top: 16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Contact Info",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.amber[600])),
                        SizedBox(height: 4),
                        Text("Email (BUU)",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('63160004.go.buu.ac.th',
                            style: TextStyle(fontSize: 14)),
                        Text("Email (general)",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('aum.jansri@gmail.com',
                            style: TextStyle(fontSize: 14)),
                        Text("Phone",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text('0986031686', style: TextStyle(fontSize: 14)),
                        Text("Adress",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                            '73/42 หมู่บ้านพรเทพการ์เด้นวิลล์ ตำบลบางพระ อำเภอศรีราชา จังหวัดชลบุรี อำเภอศรีราชา จังหวัดชลบุรี 20110',
                            style: TextStyle(fontSize: 14)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
