import 'package:flutter/material.dart';
import 'package:regbuu/table.dart';
import 'Login.dart';
import 'Profile.dart';
import 'main.dart';
import 'studentCard.dart';
import 'contact.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return buildDrawerWidget(context);
  }

  Widget buildDrawerWidget(context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.menu),
            title: Text('เมนูหลัก'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyApp(),
                  ))
            },
          ),
          ListTile(
            leading: Icon(Icons.login),
            title: Text('เข้าสู่ระบบ'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('ข้อมูลนิสิต'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Profile(),
                  ))
            },
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('บัตรนิสิต'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => StudentCard(),
                  ))
            },
          ),
          ListTile(
            leading: Icon(Icons.table_chart_rounded),
            title: Text('ตารางเรียนนิสิต'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Tables(),
                  ))
            },
          ),
          ListTile(
            leading: Icon(Icons.phone),
            title: Text('ติดต่อเรา'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Contact(),
                  ))
            },
          ),
        ],
      ),
    );
  }
}
