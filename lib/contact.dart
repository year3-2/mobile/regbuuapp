import 'package:flutter/material.dart';

class Contact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ติดต่อเรา'),
      ),
      body: BuildBodyContact(context),
    );
  }

  Widget BuildBodyContact(context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                height:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? screenHeight*0.4
                        : screenHeight*0.5,
                child: Card(
                  color: Colors.grey[50],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 8,
                  child: Container(
                    padding: EdgeInsets.only(left: 16.0, top: 16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Contact",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.amber[600])),
                        SizedBox(height: 4),
                        Text("เวลาทำการ",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                            'เปิดภาคเรียน08:30 - 19:30 น. (จันทร์ พุธ ศุกร์) 08:30 - 16:30 น. (อังคาร พฤหัสบดี เสาร์ อาทิตย์)หมายเหตุ: ปิดวันหยุดราชการ',
                            style: TextStyle(fontSize: 14)),
                        Text(
                            'ปิดภาคเรียน08:30 - 16:30 น. (จันทร์ - ศุกร์)ปิดเสาร์ - อาทิตย์',
                            style: TextStyle(fontSize: 14)),
                        Text("ที่อยู่",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                            'กองทะเบียนและประมวลผลการศึกษา มหาวิทยาลัยบูรพา 169 ถ.ลงหาดบางแสน ต.แสนสุข อ.เมือง จ.ชลบุรี 20131',
                            style: TextStyle(fontSize: 14)),
                        Text("โทรศัพท์ (เจ้าหน้าที่)",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        SizedBox(height: 4),
                        Text(
                            '	ฝ่ายรับเข้าศึกษาระดับปริญญาตรี โทร. 038-102721 หรือ 038-102643',
                            style: TextStyle(fontSize: 14)),
                        Text(
                            "ระดับบัณฑิตศึกษา โทร. 038-102724 หรือ 038-102726 ",
                            style: TextStyle(fontSize: 14)),
                        Text("คณะวิทยาการสารสนเทศ (โทร. 038-102724)",
                            style: TextStyle(fontSize: 14)),
                        Text(
                            "การเทียบโอนผลการเรียน ตรวจสอบคุณวุฒิ โทร. 038-102718",
                            style: TextStyle(fontSize: 14)),
                        Text("จำนวนนิสิตและบุคคลภายนอก (โทร. 038102719)",
                            style: TextStyle(fontSize: 14)),
                        Text("ระบบบริการการศึกษา (โทร. 038-102722)",
                            style: TextStyle(fontSize: 14)),
                        Text("งานธรุการโทร. 038-102719 โทร. 038-102719",
                            style: TextStyle(fontSize: 14)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
