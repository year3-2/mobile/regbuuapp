import 'package:flutter/material.dart';

class StudentCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('บัตรนิสิต'),
      ),
      body: BuildBodyStudentCard(context),
    );
  }

  Widget BuildBodyStudentCard(context) {
    return SingleChildScrollView(
      child:Row(
      children: [
        Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: 20,
              ),
              width: MediaQuery.of(context).size.width,
              height: 600,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: DecorationImage(
                  image: NetworkImage(
                      'https://media.discordapp.net/attachments/1043925948707381278/1070290185587277885/IMG_0374.png?width=444&height=703'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ],
        ),
      ],
    ),
    );

  }
}
